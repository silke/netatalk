#!/usr/bin/env bash
set -euo pipefail

# Common dependencies
dependencies="
autotools-dev
avahi-daemon
build-essential
cdbs
d-shlibs
debhelper
devscripts
dh-buildinfo
dh-systemd
libacl1-dev
libavahi-client-dev
libcrack2-dev
libcups2-dev
libdb-dev
libdbus-1-dev
libdbus-glib-1-dev
libevent-dev
libgcrypt-dev
libgcrypt11-dev
libglib2.0-dev
libio-socket-inet6-perl
libkrb5-dev
libldap2-dev
libltdl3-dev
libpam0g-dev
libssl-dev
libtdb-dev
libtracker-miner-1.0-dev
libtracker-sparql-1.0-dev
libwrap0-dev
systemtap-sdt-dev
tracker
"

# Get major debian version
version=$(cat /etc/debian_version | cut -d'.' -f1)

# Version compatibilities
if [ "${version}" -eq "9" ]; then
  dependencies="${dependencies} libmariadbclient-dev"
elif [ "${version}" -eq "8" ]; then
  dependencies="${dependencies} libmysqlclient-dev"
fi

# Install dependencies
apt-get update -qq
apt-get install -qq ${dependencies}

# Build deb
cd netatalk
debuild -b -uc -us
